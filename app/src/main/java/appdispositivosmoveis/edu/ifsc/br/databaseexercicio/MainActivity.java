package appdispositivosmoveis.edu.ifsc.br.databaseexercicio;

import android.app.AlertDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final int MSG_DADOS_INSERIDOS = 1;
    private final int MSG_CRIOU_BD = 2;

    private SQLiteDatabase bd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        criaBD();
        insereDados();
        listarDados();

    }

    private void listarDados() {

        Cursor cursor = bd.rawQuery("SELECT id, nome FROM alunos ",
                null);
        cursor.moveToFirst();
        //cursor.getBlob()
        ArrayList<String> itens = new ArrayList<>();
        do {
            String s = cursor.getString( cursor.getColumnIndex("nome"));
            itens.add(s);
        }while (cursor.moveToNext()) ;
        ListView list;
        list = findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String> (
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                itens
        );
        list.setAdapter(adapter);
    }


    private void insereDados() {

        bd.execSQL("INSERT INTO alunos (nome) VALUES ('Romulo Beninca' )");
        bd.execSQL("INSERT INTO alunos (nome) VALUES ('Mateus Nunes' )");
        bd.execSQL("INSERT INTO alunos (nome) VALUES ('Andreu Carminatti' )");

        Mensagem(MSG_DADOS_INSERIDOS);

    }

    private void criaBD() {

        bd = openOrCreateDatabase("banco", MODE_PRIVATE,null);
        bd.execSQL("CREATE TABLE IF NOT EXISTS alunos ( "+
                   "                id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                   " nome VARCHAR )" );
        Mensagem(MSG_CRIOU_BD);


    }

    private void Mensagem(int MSG_CODE) {
        switch(MSG_CODE) {
            case MSG_DADOS_INSERIDOS:
                AlertDialog.Builder messageBox = new AlertDialog.Builder(this);
                messageBox.setMessage("Dados Incluídos com Sucesso!");
                messageBox.setTitle("Aviso");
                messageBox.setNeutralButton("OK", null);
                messageBox.show();
                break;
            case MSG_CRIOU_BD:
                AlertDialog.Builder messageBox2 = new AlertDialog.Builder(this);
                messageBox2.setMessage("Banco de Dados Criado ou Aberto com Sucesso!");
                messageBox2.setTitle("Aviso");
                messageBox2.setNeutralButton("OK", null);
                messageBox2.show();
                break;
        }
    }
}
